

package br.com.myproject;

import static org.hamcrest.CoreMatchers.containsString;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;

@QuarkusTest
public class MyProjectEndpointTest  {

	@Test
	public void getAllMyProjectTest() {
		RestAssured.
			when()
			.get("/api/v1/myproject").then().statusCode(200)
			.contentType("application/json")
			.body(containsString("Corote"));
	}

	@Test
	public void updateProductTest() {
		String myJson = "{\"otherInfo\":\"Pinga certa: " + LocalDateTime.now() + "\"}";
		RestAssured.given().body(myJson).contentType("application/json").when().put("/api/v1/myproject/5").then()
				.statusCode(200);
	}
}