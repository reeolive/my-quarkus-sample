
package br.com.myproject.service;

import br.com.myproject.entity.MyProjectEntity;
import br.com.myproject.model.MyProjectRequestModel;
import br.com.myproject.model.MyProjectResponseModel;
import br.com.myproject.model.PageMyProjectResponseModel;
import io.smallrye.mutiny.Uni;

public interface MyProjectService {

	Uni<Long> count();

	Uni<PageMyProjectResponseModel> findAll(int page, int size);

	Uni<MyProjectResponseModel> findById(Long id);

	Uni<MyProjectEntity> save(MyProjectRequestModel productModel);

	Uni<MyProjectResponseModel> update(MyProjectRequestModel productModelRequest, Long id);

	Uni<Boolean> delete(Long i);

}
