
package br.com.myproject.service;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import java.time.LocalDateTime;
import java.util.function.Function;

import br.com.myproject.entity.MyProjectEntity;
import br.com.myproject.mapper.MyProjectMapper;
import br.com.myproject.model.MyProjectRequestModel;
import br.com.myproject.model.MyProjectResponseModel;
import br.com.myproject.model.PageMyProjectResponseModel;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

@Dependent
public class MyProjectServiceImpl extends MyProjectSupportPage implements MyProjectService {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MyProjectServiceImpl.class.getName());

	@Inject
	MyProjectMapper myprojectMapper;

	@Override
	public Uni<PageMyProjectResponseModel> findAll(final int pageIndex, final int size) {
		PageMyProjectResponseModel pageMyProjectResponseModel = PageMyProjectResponseModel.builder().pageable(this.buildPage(pageIndex, size)).build();

		Function<Long, Uni<? extends PageMyProjectResponseModel>> callPagination = total -> {
			pageMyProjectResponseModel.setTotalElements(total);
			pageMyProjectResponseModel.setTotalPages(this.getTotalPage(size, total));

			Multi<MyProjectEntity> retorno = MyProjectEntity.findAll(Sort.ascending("id")).page(pageIndex, size).stream();
			retorno.subscribe().with(item -> pageMyProjectResponseModel.addContentItem(myprojectMapper.toModel(item)));

			return retorno.toUni().replaceWith(pageMyProjectResponseModel);
		};

		return MyProjectEntity.count().onItem().transformToUni(callPagination);					
	}

	@Override
	public Uni<MyProjectResponseModel> findById(Long id) {
		Uni<MyProjectEntity> myprojectEntity = MyProjectEntity.findById(id);
		return myprojectMapper.toResponseModel(myprojectEntity);
	}

	@Override
	public Uni<MyProjectEntity> save(MyProjectRequestModel productModel) {
		MyProjectEntity entity = myprojectMapper.toEntity(productModel, null);
		entity.setModifiedAt(LocalDateTime.now());
		
		return Panache.withTransaction(entity::persist);
	}

	@Override
	public Uni<MyProjectResponseModel> update(MyProjectRequestModel productModel, Long id) {
		return Panache.withTransaction(
				() -> myprojectMapper.toResponseModel(MyProjectEntity.<MyProjectEntity>findById(id).onItem()
						.ifNotNull().invoke(entityUpdate -> entityUpdate.setOtherInfo(productModel.getOtherInfo()))));
	}

	@Override
	public Uni<Boolean> delete(Long id) {
		return Panache.withTransaction(() -> MyProjectEntity.deleteById(id));
	}

	@Override
	public Uni<Long> count() {
		return MyProjectEntity.count();
	}

}
