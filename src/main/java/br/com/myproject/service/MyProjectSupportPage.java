
package br.com.myproject.service;

import br.com.myproject.model.Pageable;

public class MyProjectSupportPage {

	public Pageable buildPage(final int page, final int size) {
		return Pageable.builder().offset(page * size).pageSize(size).pageNumber(page).build();

	}

	public long getTotalPage(final int size, final long total) {
		return total % size == 0 ? (total / size) : (total / size) + total % size;
	}

}
