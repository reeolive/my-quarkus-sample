

package br.com.myproject.mapper;

import java.util.Optional;
import java.util.function.Function;

import javax.enterprise.context.Dependent;

import br.com.myproject.entity.MyProjectEntity;
import br.com.myproject.model.MyProjectRequestModel;
import br.com.myproject.model.MyProjectResponseModel;
import br.com.myproject.model.PageMyProjectResponseModel;
import io.smallrye.mutiny.Uni;

@Dependent
public class MyProjectMapper {

	public MyProjectResponseModel toModel(final MyProjectEntity myprojectEntity) {

		Optional<MyProjectResponseModel> myProjectResponseModel = Optional
				.ofNullable(myprojectEntity == null ? null : new MyProjectResponseModel());

		if (!myProjectResponseModel.isPresent()) {
			return MyProjectResponseModel.builder().build();
		}

		Optional<MyProjectEntity> entity = Optional.ofNullable(myprojectEntity);

		Optional.ofNullable(entity.get().getId()).ifPresent(id -> 
			myProjectResponseModel.get().setId(id)
		);

		Optional.ofNullable(entity.get().getOtherInfo()).ifPresent(otherInfo -> 
			myProjectResponseModel.get().setOtherInfo(otherInfo)
		);

		return myProjectResponseModel.get();
	}

	public MyProjectEntity toEntity(final MyProjectRequestModel myprojectRequestModel, final Long id) {
		return MyProjectEntity.builder().id(id).otherInfo(myprojectRequestModel.getOtherInfo()).build();
	}

	public PageMyProjectResponseModel buildContentModelPagination(
			final PageMyProjectResponseModel pageMyProjectResponseModel,
			final MyProjectResponseModel myprojectResponseModel) {
		pageMyProjectResponseModel.addContentItem(myprojectResponseModel);
		return pageMyProjectResponseModel;
	}
	
	
	public Uni<MyProjectResponseModel> toResponseModel(final Uni<MyProjectEntity> appArsenalEntity) {

		MyProjectResponseModel myProjectResponseModel = new MyProjectResponseModel();
		Function<Long, Uni<? extends MyProjectResponseModel>> returnResponseModel = total -> {

			appArsenalEntity.subscribe().with(item -> myProjectResponseModel.setId(item.getId()));
			appArsenalEntity.subscribe().with(item -> myProjectResponseModel.setOtherInfo(item.getOtherInfo()));

			return appArsenalEntity.replaceWith(myProjectResponseModel);
		};

		return MyProjectEntity.count().onItem().transformToUni(returnResponseModel);
	}

}
