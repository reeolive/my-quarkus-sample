package br.com.myproject.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.smallrye.mutiny.Uni;
import br.com.myproject.client.ClientlResponseModel;
import br.com.myproject.client.MyProjectRestClient;
import br.com.myproject.model.MyProjectResponseModel;

@RequestScoped
@Path("/api/v1/apparsenal/rest-client")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class RestClientExampleController {

	@Inject
	@RestClient
	private MyProjectRestClient client;

	@GET
	@Path("{id}")
	public Uni<MyProjectResponseModel> findById(@PathParam("id") Long id) {
		ClientlResponseModel ret = client.getMyProjectModel(id);

		MyProjectResponseModel model = new MyProjectResponseModel();
		model.setId(ret.getId());
		model.setOtherInfo(ret.getOtherInfo());
		return Uni.createFrom().item(model);
	}

}
