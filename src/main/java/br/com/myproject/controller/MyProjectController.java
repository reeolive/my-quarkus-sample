
package br.com.myproject.controller;

import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.ok;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import io.quarkus.arc.log.LoggerName;
import io.smallrye.mutiny.Uni;

import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse.StatusCode;

import br.com.myproject.model.MyProjectRequestModel;
import br.com.myproject.service.MyProjectService;
import br.com.myproject.service.MyProjectSupportPage;

@RequestScoped
@Path("/api/v1/myproject/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MyProjectController extends MyProjectSupportPage {

	/** Logger. */
	@Inject
	@LoggerName("MyProject")
	Logger logger;

	@Inject MyProjectService myProjectService;

	@POST
	public Uni<Response> create(@Valid MyProjectRequestModel productModelRequest){
		
		logger.info("Creating MyProject...");
		return myProjectService.save(productModelRequest)
				.map(responseModel -> Response.ok(responseModel).status(StatusCode.CREATED).build());
	}

	@GET
	@Path("{id}")
	public Uni<Response> get(@PathParam("id") final Long id) {
		
		logger.infof("Searching for MyProject with ID: $d ", id);
		return myProjectService.findById(id).map(data -> {
			if (data.getId() == null) {
				return null;
			}
			return ok(data).build();
		}).onItem().ifNull().continueWith(status(Status.NOT_FOUND).build());
	}

	@GET
	public Uni<Response> get(@QueryParam(value = "page") @DefaultValue("0") int page, @QueryParam(value = "size") @DefaultValue("10") int size) {
		
		logger.info("Listing all MyProject");
		return myProjectService.findAll(page, size)
				.map(responseModel -> Response.ok(responseModel).status(StatusCode.OK).build());
	}

	@PUT
	@Path("{id}")
	public Uni<Response> update(@PathParam("id") final Long id, MyProjectRequestModel productModelRequest){
		
		logger.infof("Updating MyProject with ID: $d ", id);
		return myProjectService.update(productModelRequest, id).map(model -> {
			if (model.getId() == null) {
				return null;
			}
			return Response.ok(model).status(StatusCode.OK).build();
		}).onItem().ifNull().continueWith(status(StatusCode.NOT_FOUND).build());

	}

	@DELETE
	@Path("{id}")
	public Uni<Response> delete(@PathParam("id") final Long id) {
		
		logger.infof("Deleting MyProject with ID: $d ", id);
		return myProjectService.delete(id).map(deleted -> deleted ? Response.ok().status(StatusCode.NO_CONTENT).build()
				: Response.ok().status(StatusCode.NOT_FOUND).build());
	}

}
