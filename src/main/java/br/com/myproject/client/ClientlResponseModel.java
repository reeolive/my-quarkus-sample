package br.com.myproject.client;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Data Transfer Object (DTO) representing Appresponses. */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientlResponseModel implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The App identifier. */
  private long id;

  /** Other info about the App */
  private String otherInfo;
}
