package br.com.myproject.client;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

//change the path to match your api to be consumed 
@Path("/api/v1/app")
@RegisterRestClient
public interface MyProjectRestClient {

	//change path param to match your variables to be provided
	//change Object type to match api response
	@GET
	@Path("/{id}") 
	ClientlResponseModel getMyProjectModel(@PathParam(value = "id") Long id);

}
