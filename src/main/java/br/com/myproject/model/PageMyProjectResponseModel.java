
package br.com.myproject.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageMyProjectResponseModel {

	private List<MyProjectResponseModel> content;

	private Pageable pageable;

	private boolean last;

	private int size;

	private int number;

	private boolean empty;

	private Sort sort;

	private long totalElements;

	private long totalPages;

	private boolean first;

	public PageMyProjectResponseModel content(List<MyProjectResponseModel> content) {
		this.content = content;
		return this;
	}

	public PageMyProjectResponseModel addContentItem(MyProjectResponseModel contentItem) {
		if (this.content == null) {
			this.content = new ArrayList<>();
		}
		this.content.add(contentItem);
		return this;
	}

}
