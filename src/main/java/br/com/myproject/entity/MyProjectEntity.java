
package br.com.myproject.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MyApp")
@NamedQuery(name = "MyApp.findAll", query = "SELECT myapp FROM MyProjectEntity myapp ORDER BY myapp.otherInfo")
public class MyProjectEntity extends PanacheEntityBase {

	@Id
	@SequenceGenerator(name = "AppSequence", sequenceName = "App_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator = "AppSequence")
	private Long id;

	@Column(length = 40, unique = false)
	private String otherInfo;

	@Builder.Default
	@Column(name = "created_at", nullable = false, updatable = false)
	LocalDateTime createdAt = LocalDateTime.now();

	@Column(name = "modified_at", nullable = false)
	LocalDateTime modifiedAt;

}
